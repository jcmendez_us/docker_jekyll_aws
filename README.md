# Jekyll and aws-cli image

This is a Docker image to build static websites using jekyll and push
them to aws

[![DockerHub Badge](http://dockeri.co/image/jcmendez/jekyll_aws_builder)](https://hub.docker.com/r/jcmendez/jekyll_aws_builder/)

## Build

```
docker build -t jcmendez/aws-cli .
```

## Usage

On your development machine, on the directory where the jekyll site is
```
docker run --rm --volume="$PWD:/srv/jekyll" -p 35729:35729 -p 4000:4000 -it jcmendez/jekyll_aws_builder:v1 jekyll serve
```

```
docker run --rm --volume="$PWD:/srv/jekyll" -it jcmendez/jekyll_aws_builder:v1 jekyll build
```

AWS needs the following ENV variables, that need to be set

```
AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY
AWS_DEFAULT_REGION
```

## Similar projects

https://github.com/mesosphere/aws-cli

