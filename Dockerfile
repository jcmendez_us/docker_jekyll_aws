FROM jekyll/builder
MAINTAINER Juan C. Mendez <jcmendez@alum.mit.edu>

RUN apk --no-cache update && \
    apk add --no-cache curl && \
    apk add --no-cache python python-dev py-pip py-setuptools ca-certificates && \
    apk add --no-cache curl groff && \
    pip install --upgrade pip && \
    pip --no-cache-dir install awscli && \    
    rm -r /root/.cache && \
    rm -rf /var/cache/apk/*

